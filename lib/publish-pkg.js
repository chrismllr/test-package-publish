const fs = require('fs');
const os = require('os');

const { publish } = require('libnpmpublish');

const GITLAB_TOKEN = 'zbzV6b9cyTcFA2aGws_c'; 

async function publishPackageAtVersion(pkg, version) {
  const pkgJson = require(`${os.homedir()}/repos/loom-ember-gl/packages/${pkg}/package.json`);
  const tarball = fs.createReadStream(`${os.homedir()}/Downloads/${pkg}-${version}.tgz`);

  pkgJson.version = version;
    
  try {
    const npmVersion = `@loom/${pkg}@${version}`;
    const registry = 'https://gitlab.toyotaconnected.net/api/v4/projects/972/packages/npm/'
    const output = await publish(pkgJson, tarball, {
      npmVersion,
      token: GITLAB_TOKEN,     
      registry   
    });

    console.log('Successful publish:', npmVersion);
  } catch(err) {
    console.error('error publishing:', err);
    throw err;
  }

}

module.exports = publishPackageAtVersion;
