const publish = require('./lib/publish-pkg.js');
const { argv } = require('yargs');

const { v: version, pkg } = argv;

(async function publishMultipleVersions() {
  console.log('Publishing version(s):', version, '\n');

  const errs = [];

  if (Array.isArray(version)) {
    for (const v of version) {
      try {
        await publish(pkg, v);
      } catch(err) {
        errs.push({ pkg: `${pkg}@${v}`, reason: err });     
      }
    }
  } else {
    try {
      await publish(pkg, version);
    } catch(err) {
      errs.push({ pkg: `${pkg}@${version}`, reason: err });     
    }
  }

  console.log('Failures:\n', errs);
})();
